$("document").ready(function () {

    //példának kell
    $("#datas").click(function () {

        $.ajax({
            method: "GET",
            url: "printer-list-table-ajax",
            success: function (res) {
                $("#printerlist-table").html(res);
            }
        })
});
    function refreshPrinterList() {
        $.ajax({
            method: "GET",
            url: "printer-list-table-ajax",
            success: function (res) {
                $("#printerlist-table").html(res);
            }
        })
    }
    $("body").on("click", ".change-password", function () {
        self = $(this);
        $.ajax({
            url: "change-password-dialog",
            method: "GET",
            data: {"printer": self.data("id")},
            success: function (reply) {
                var newDiv = $(document.createElement('div'));
                newDiv.html(reply);
                dialog = newDiv.dialog({
                    height: 470,
                    width: 350,
                    modal: true,
                    title: "Jelszócsere",
                    buttons: {
                        Mentés: function () {


                            if($("#key_id").val()===($("#key2_id").val()) && $("#key_id").val()!="" && $("#key2_id").val()!="") {

                                $.ajax({
                                    url: "save-new-password",
                                    data: $("#changePasswordForm").serialize(),
                                    method: "POST",
                                    success: function () {
                                        refreshPrinterList();
                                        dialog.dialog("close");
                                    }
                                })

                            }
                            else
                                {
                                    $('.error').css("visibility","visible");
                                }




                        },
                        Mégse: function () {
                            dialog.dialog("close");
                        }
                    },
                    close: function () {
                        $(this).remove();
                    }
                });
            }
        })
    });
    $("body").on("click", ".printerDelete", function () {

        self = $(this);
        var newDiv = $(document.createElement('div'));
        newDiv.html("<br>" +
            "<p style='text-align: center'>Törli a kijelölt nyomtatót? </p>");

        dialog = newDiv.dialog({
            height: 150,
            width: 250,
            modal: true,
            title: "Delete printer",
            buttons: {
                Igen: function () {
                        $.ajax({
                            url: "deletePrinter",
                            method: "POST",
                            data: {"printer_id": self.data("id")},
                            success: function (e) {
                                refreshPrinterList();
                                dialog.dialog("close");
                            }
                        });

                },
                Nem: function () {
                    dialog.dialog("close");
                }
            },
            close: function () {
                $(this).remove();
            }

        });
        dialog.css("backgroundColor","red");


    });
    $("body").on("click", ".printerActive", function () {
        self = $(this);
        $.ajax({
            url: "activatePrinter",
            method: "POST",
            data: {"printer": self.data("id")},
            success: function (e) {
                refreshPrinterList();
            }
        });
    });
});