CREATE TABLE table_serials (
  serial_id int(11) NOT NULL AUTO_INCREMENT,
  title mediumtext NOT NULL,
  rate double NOT NULL,
  description mediumtext NOT NULL,
  PRIMARY KEY (serial_id)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
INSERT INTO `table_serials` VALUES (2,'4asd',2,'An animated series that follows the exploits of a super scientist and his not-so-bright grandson.'),(3,'Rick and Morty',3,'An animated series that follows the exploits of a super scientist and his not-so-bright grandson.'),(4,'Rick and Morty',5,'Rick and MortyRick and MortyRick and Morty'),(5,'Rick and Morty',5,'Rick and MortyRick and MortyRick and MortyRick and MortyRick and Morty'),(6,'Rick and Morty',5,'Rick and MortyRick and MortyRick and MortyRick and Morty');

CREATE TABLE `role` (
    `role_id` int(11) NOT NULL AUTO_INCREMENT,
    `role` varchar(255) DEFAULT NULL,
    PRIMARY KEY (`role_id`)
    ) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `user` (
    `user_id` int(11) NOT NULL AUTO_INCREMENT,
    `active` int(11) DEFAULT NULL,
    `email` varchar(255) NOT NULL,
    `password` varchar(255) NOT NULL,
    `username` varchar(255) NOT NULL,
    PRIMARY KEY (`user_id`)
    ) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `user_role` (
    `user_id` int(11) NOT NULL,
    `role_id` int(11) NOT NULL,
    PRIMARY KEY (`user_id`,`role_id`),
    KEY `FKa68196081fvovjhkek5m97n3y` (`role_id`),
    CONSTRAINT `FK859n2jvi8ivhui0rl0esws6o` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`),
    CONSTRAINT `FKa68196081fvovjhkek5m97n3y` FOREIGN KEY (`role_id`) REFERENCES `role` (`role_id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
INSERT INTO `role` VALUES (1,'ADMIN');
