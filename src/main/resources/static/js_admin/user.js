$('#datetimepicker1').datetimepicker({
    format: 'YYYY-MM-DD HH:mm:ss'
});
$(document).ready(function(){
    var lang = {
        "sEmptyTable":     "Nincs rendelkezésre álló adat",
        "sInfo":           "Találatok: _START_ - _END_ Összesen: _TOTAL_",
        "sInfoEmpty":      "Nulla találat",
        "sInfoFiltered":   "(_MAX_ összes rekord közül szűrve)",
        "sInfoPostFix":    "",
        "sInfoThousands":  " ",
        "sLengthMenu":     "_MENU_ oldalanként",
        "sLoadingRecords": "Betöltés...",
        "sProcessing":     "Feldolgozás...",
        "sSearch":         "Keresés:",
        "sZeroRecords":    "Nincs a keresésnek megfelelő találat",
        "oPaginate": {
            "sFirst":    "Első",
            "sPrevious": "Előző",
            "sNext":     "Következő",
            "sLast":     "Utolsó"
        },
        "oAria": {
            "sSortAscending":  ": aktiválja a növekvő rendezéshez",
            "sSortDescending": ": aktiválja a csökkenő rendezéshez"
        }
    };
    var table = loadDT(table);
    $(document).on('click', "#btn_category_add", function() {
       if($("#DATA").valid())
       {
           const form = document.forms['DATA'];
           var formData = new FormData(form);
           let premiumOrNot = $('input[name="tipp_type"]:checked').val();
           formData.append('premiumOrNot',premiumOrNot);
           formData.append('date',$('#date_time').val().trim());
           if(form.checkValidity())
           {
               $.ajax({
                   method:'POST',
                   url: '/tipp/add',
                   data: formData,
                   processData: false,
                   contentType: 'application/json',
                   success: function(data){
                       if(data.type=='success')
                       {
                           $('#myModal').modal('hide');
                           loadDT(table);
                       }
                       else if(data.type=='error')
                       {
                            alert('Sikertelen felvitel!');
                       }
                   },
                   error: function (error) {
                       alert(error);
                   }
               });
           }
       }

    });
    $('#btn_add_category').on('click',function (event) {
        $('#myModal').modal('show');
    });
    function loadDT(table_input){
        $.ajax({
            method: 'POST',
            url : '/user/getAll',
            processData: false,
            contentType: false,
            destroy: true,
            success : function (obj) {
                if(table!=null || table!=undefined)
                {
                    table.destroy();
                }
                table_input = $('#dt_user').DataTable({
                    data: obj,
                    dom: '<"wrapper"tp>',
                    language : lang,
                    pageLength : 10,
                    columns: [
                        {
                            data: 'id' },
                        {
                            data: 'email'
                        },
                        {
                            data: 'username'
                        },
                        {
                            data:'full_name'
                        },
                        {
                          data: 'pay_id'
                        },
                        {
                            data: 'active',
                            render:function (data) {
                                if(data==1)
                                {
                                    return '<i class="fa text-success fa-check-circle"/>';
                                }
                                else return '<i class="fa text-danger fa-minus-circle"/>';

                            }
                        },
                        {
                            data: 'roles',
                            render:function(data){
                                var text = "";
                                $.each(data,function (k,v) {
                                   text=text+'<span class=" font-weight-bold ml-2">'+v.role+'<span>';
                                });
                                return text;
                            },
                        },
                        {
                            data:'membership'
                        },
                        {
                            data:'user_created'
                        },
                        {
                            data:'address'
                        },
                        {

                            data:'id',
                            orderable:false,
                            searchable:false,
                            render : function (data) {
                                return '<i class="ml-2 fa fa-trash status_delete" data-id='+data+'></i>'+
                                       '<i class="ml-2 fa fa-lg fa-toggle-on  btn-sm text-danger" id="btn_status" data-id='+data+'></i>';
                            }
                        }
                    ]
                });
                table = table_input;
            }
        });
    };
    $(document).on('click','.status_delete',function () {
        $.ajax({
            method: 'POST',
            url : '/user/deleteUser/'+$(this).data('id'),
            processData: false,
            contentType: false,
            destroy: true,
            success : function (data) {
                if(data.type=='success')
                {
                    alert('Sikeres');
                    loadDT(table);
                }
                else if(data.type=='error')
                {
                    alert('Sikertelen');
                }
            }
        });
    });
    $(document).on('click','#btn_status',function (data) {
        const form =  new FormData();
        form.append('id',$(this).data('id'));
        $.ajax({
            method: 'POST',
            url : '/user/changeStatus',
            data: form,
            processData: false,
            contentType: false,
            destroy: true,
            success : function (data) {
                if(data.type=='success')
                {
                    alert('Sikeres');
                    loadDT(table);
                }
                else if(data.type=='error')
                {
                    alert('Sikertelen');
                }
            }
        });
    });

});