$('#datetimepicker1').datetimepicker({
    format: 'YYYY-MM-DD HH:mm:ss'
});
$(document).ready(function(){
    $.extend($.validator.messages, {
        required: "<label class='control-label'>Kitöltendő!</label>"
    });
    var lang = {
        "sEmptyTable":     "Nincs rendelkezésre álló adat",
        "sInfo":           "Találatok: _START_ - _END_ Összesen: _TOTAL_",
        "sInfoEmpty":      "Nulla találat",
        "sInfoFiltered":   "(_MAX_ összes rekord közül szűrve)",
        "sInfoPostFix":    "",
        "sInfoThousands":  " ",
        "sLengthMenu":     "_MENU_ oldalanként",
        "sLoadingRecords": "Betöltés...",
        "sProcessing":     "Feldolgozás...",
        "sSearch":         "Keresés:",
        "sZeroRecords":    "Nincs a keresésnek megfelelő találat",
        "oPaginate": {
            "sFirst":    "Első",
            "sPrevious": "Előző",
            "sNext":     "Következő",
            "sLast":     "Utolsó"
        },
        "oAria": {
            "sSortAscending":  ": aktiválja a növekvő rendezéshez",
            "sSortDescending": ": aktiválja a csökkenő rendezéshez"
        }
    };
    var table = loadDT(table);
    var tableClosed = loadDTDone(tableClosed);
    $(document).on('click', "#btn_category_add", function() {
       if($("#DATA").valid())
       {
           var form = document.forms['DATA'];
           const formData = new FormData(form);
           var premiumOrNot = $('input[name="tipp_type"]:checked').val();
           formData.append('premiumOrNot',premiumOrNot);
           formData.append('date',$('#date_time').val().trim());
           if(form.checkValidity())
           {
               $.ajax({
                   method:'POST',
                   url: '/tipp/add',
                   data: formData,
                   processData: false,
                   contentType: false,
                   success: function(data){
                       if(data.type=='success')
                       {
                           $('#myModal').modal('hide');
                           loadDT(table);
                       }
                       else if(data.type=='error')
                       {
                            alert('Sikertelen felvitel!');
                       }
                   },
                   error: function (error) {
                       alert(error);
                   }
               });
           }
       }

    });
    $('#btn_add_category').on('click',function (event) {
        $('#myModal').modal('show');
    });
    function loadDT(table_input){
        $.ajax({
            method: 'POST',
            url : '/tipp/getOpenedforAdmin',
            processData: false,
            contentType: false,
            destroy: true,
            success : function (obj) {
                if(table!=null || table!=undefined)
                {
                    table.destroy();
                }
                table_input = $('#dt_tipps_opened').DataTable({
                    data: obj,
                    dom: '<"wrapper"tp>',
                    language : lang,
                    pageLength : 10,
                    columnDefs: [
                        {
                            targets: 7,
                            className: 'bg-white'
                        }
                    ],
                    order: [[ 2, 'desc' ]],
                    columns: [
                        {
                            data: 'tipp_id' },
                        {
                            data: 'premium',
                            render:function (data) {
                                if(data==true)
                                {
                                    return '<span class="font-weight-bolder">Prémium <i class="fa fa-dollar"/></span>';
                                }
                                else return '<span class="">Ingyenes</span>';

                            }
                        },
                        {
                            data: 'tipp_generated'
                        },
                        {
                            data: 'player1'
                        },
                        {
                            data: 'player2'
                        },
                        {
                          data:'odds'
                        },
                        {
                            data:'hazaVvendeg'
                        },
                        {

                            data:'tipp_id',
                            orderable:false,
                            searchable:false,
                            render : function (data) {
                                const tipp_status = '<i class="ml-1 fas fa-heart text-success btn-sm status_success" style="cursor:pointer;" data-id='+data+'></i>' +
                                             '<i class="ml-1 fas fa-heart-broken text-danger btn-sm status_lose" style="cursor:pointer;" data-id='+data+'></i>' +
                                             '<i class="ml-1 fas fa-sm fa-piggy-bank text-warning btn-sm status_money" style="cursor:pointer;" data-id='+data+'></i>';
                                return  '<i class="ml-1 fas fa-info-circle text-info details-control  btn-sm" style="cursor:pointer;"></i>' +
                                    '<i class="ml-1 fas fa-trash  btn-sm status_delete" style="cursor:pointer;" data-id='+data+'></i>'+tipp_status;
                            }
                        }
                    ]
                });
                table = table_input;
            }
        });
    };
    function loadDTDone(table_input){
        $.ajax({
            method: 'POST',
            url : '/tipp/getClosedforAdmin',
            processData: false,
            contentType: false,
            destroy: true,
            success : function (obj) {
                if(tableClosed!=null || tableClosed!=undefined)
                {
                    tableClosed.destroy();
                }
                table_input = $('#dt_tipps_closed').DataTable({
                    data: obj,
                    dom: '<"wrapper"tp>',
                    language : lang,
                    pageLength : 10,
                    columnDefs: [
                        {
                            targets: 7,
                            className: 'bg-white'
                        }
                    ],
                    order: [[ 2, 'desc' ]],
                    createdRow: function( row , data) {
                        let $vOl = data.winOrLose;
                        if($vOl==1)
                        {
                            $(row).addClass('bg-success');
                        }
                        else if($vOl==2)
                        {
                            $(row).addClass('bg-danger');
                        }
                        else if($vOl==3)
                        {
                            $(row).addClass('bg-warning');
                        }
                    },
                    columns: [
                        {
                            data: 'tipp_id' },
                        {
                            data: 'premium',
                            render:function (data) {
                                if(data==true)
                                {
                                    return '<span class="font-weight-bolder">Prémium <i class="fa fa-dollar"/></span>';
                                }
                                else return '<span class="">Ingyenes</span>';

                            }
                        },
                        {
                            data: 'tipp_generated'
                        },
                        {
                            data: 'player1'
                        },
                        {
                            data: 'player2'
                        },
                        {
                            data:'odds'
                        },
                        {
                            data:'hazaVvendeg'
                        },
                        {

                            data:'tipp_id',
                            orderable:false,
                            searchable:false,
                            render : function (data) {
                                const tipp_status = '<i class="ml-1 fas fa-heart text-success btn-sm status_success" style="cursor:pointer;" data-id='+data+'></i>' +
                                    '<i class="ml-1 fas fa-heart-broken text-danger btn-sm status_lose" style="cursor:pointer;" data-id='+data+'></i>' +
                                    '<i class="ml-1 fas fa-sm fa-piggy-bank text-warning btn-sm status_money" style="cursor:pointer;" data-id='+data+'></i>';
                                return  '<i class="ml-1 fas fa-info-circle text-info details-control  btn-sm" style="cursor:pointer;"></i>' +
                                    '<i class="ml-1 fas fa-trash  btn-sm status_delete" style="cursor:pointer;" data-id='+data+'></i>'+tipp_status;
                            }
                        }
                    ]
                });
                tableClosed = table_input;
            }
        });
    };
    $(document).on('click','.status_success',function () {
        let $data = $(this).data('id');
        $.ajax({
            method: 'POST',
            url : '/tipp/setSuccess/'+$data,
            processData: false,
            contentType: false,
            destroy: true,
            success : function (data) {
                if(data.type=='success')
                {
                    alert('Sikeres módosítás!');
                    reloadDataTables();
                }
                else if(data.type=='error')
                {
                    alert('Sikertelen felvitel!');
                }
            },
            error:function (obj) {
             alert('Ajax hiba!');
            }
            });
    });
    $(document).on('click','.status_lose',function () {
        let $data = $(this).data('id');
        $.ajax({
            method: 'POST',
            url : '/tipp/setLose/'+$data,
            processData: false,
            contentType: false,
            destroy: true,
            success : function (data) {
                if(data.type=='success')
                {
                    alert('Sikeres módosítás!');
                    reloadDataTables();
                }
                else if(data.type=='error')
                {
                    alert('Sikertelen felvitel!');
                }
            },
            error:function (obj) {
                alert('Ajax hiba!');
            }
        });
    });
    $(document).on('click','.status_money',function () {
        let $data = $(this).data('id');
        $.ajax({
            method: 'POST',
            url : '/tipp/setMoney/'+$data,
            processData: false,
            contentType: false,
            destroy: true,
            success : function (data) {
                if(data.type=='success')
                {
                    alert('Sikeres módosítás!');
                    reloadDataTables();
                }
                else if(data.type=='error')
                {
                    alert('Sikertelen felvitel!');
                }
            },
            error:function (obj) {
                alert('Ajax hiba!');
            }
        });
    });
    $(document).on('click', ".details-control", function() {
        var tr = $(this).closest('tr');
        var row = table.row( tr );

        if ( row.child.isShown() ) {
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            row.child( format(row.data()) ).show();
            tr.addClass('shown');
        }

    });
    function format ( d ) {
        return '<table id="dataTable" class="table table-bordered" style="width: 100%">'+
            '<tr class="thead-dark">' +
            '<th>Rövid leírás</th>' +
            '<th>Hosszú leírása</th>' +
            '</tr>'+
            '<td><p class="post">'+d.short_description+'</p></td>'+
            '<td><p class="post">'+d.description+'</p></td>'+
            '</table>';
    };
    $(document).on('click','.status_delete',function (data) {
        const form =  new FormData();
        form.append('id',$(this).data('id'));
        $.ajax({
            method: 'POST',
            url : '/tipp/delete',
            data: form,
            processData: false,
            contentType: false,
            destroy: true,
            success : function (data) {
                if(data.type=='success')
                {
                    alert('Sikeres');
                    reloadDataTables();
                }
                else if(data.type=='error')
                {
                    alert('Sikertelen');
                }
            }
        });
    });
    $(document).on('click','#div_premium',function (data) {
        if ($('#div_short').is(':visible'))
        {
            $('#div_short').slideUp(1000);
        }
    });
    $(document).on('click','#div_free',function (data) {
        if ($('#div_short').is(':hidden'))
        {
            $('#div_short').slideDown(1000);
        }
    });
    function reloadDataTables()
    {
      loadDT(table);
      loadDTDone(tableClosed);
    }
});