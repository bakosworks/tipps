$(document).ready(function(){
    var lang = {
        "sEmptyTable":     "Nincs rendelkezésre álló adat",
        "sInfo":           "Találatok: _START_ - _END_ Összesen: _TOTAL_",
        "sInfoEmpty":      "Nulla találat",
        "sInfoFiltered":   "(_MAX_ összes rekord közül szűrve)",
        "sInfoPostFix":    "",
        "sInfoThousands":  " ",
        "sLengthMenu":     "_MENU_ oldalanként",
        "sLoadingRecords": "Betöltés...",
        "sProcessing":     "Feldolgozás...",
        "sSearch":         "Keresés:",
        "sZeroRecords":    "Nincs a keresésnek megfelelő találat",
        "oPaginate": {
            "sFirst":    "Első",
            "sPrevious": "Előző",
            "sNext":     "Következő",
            "sLast":     "Utolsó"
        },
        "oAria": {
            "sSortAscending":  ": aktiválja a növekvő rendezéshez",
            "sSortDescending": ": aktiválja a csökkenő rendezéshez"
        }
    };
    var table = loadDT(table);
    $(document).on('click', "#btn_category_add", function() {
       if($("#DATA").valid())
       {
           var form = document.forms['DATA'];
           const formData = new FormData(form);
           if(form.checkValidity())
           {
               $.ajax({
                   method:'POST',
                   url: '/membership/add',
                   data: formData,
                   processData: false,
                   contentType: false,
                   success: function(data){
                       if(data.type=='success')
                       {
                           $('#myModal').modal('hide');
                           loadDT(table);
                       }
                       else if(data.type=='error')
                       {
                            alert('Sikertelen felvitel!');
                       }
                   },
                   error: function (error) {
                       alert(error);
                   }
               });
           }
       }

    });
    $('#btn_add_category').on('click',function (event) {
        $('#myModal').modal('show');
    });
    function loadDT(table_input){
        $.ajax({
            method: 'POST',
            url : '/membership/getAll',
            contentType: "application/json; charset=utf-8",
            processData: false,
            contentType: false,
            destroy: true,
            success : function (obj) {
                if(table!=null || table!=undefined)
                {
                    table.destroy();
                }
                table_input = $('#dt_membership').DataTable({
                    data: obj,
                    dom: '<"wrapper"tp>',
                    language : lang,
                    pageLength : 10,
                    columns: [
                        {
                            data: 'membership_id' },
                        {
                            data: 'name'
                        },
                        {
                            data: 'price'
                        },
                        {
                            data: 'generated_date'
                        },
                        {

                            data:'membership_id',
                            orderable:false,
                            searchable:false,
                            render : function (data) {
                                return  '<i class="fa fa-info-circle text-info details-control btn-default"></i>' +
                                    '<i class="ml-1 fa fa-trash btn-default btn-remove" data-id='+data+'></i>';
                            }
                        }
                    ]
                });
                table = table_input;
            }
        });
    };
    $(document).on('click', ".details-control", function() {
        var tr = $(this).closest('tr');
        var row = table.row( tr );

        if ( row.child.isShown() ) {
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            row.child( format(row.data()) ).show();
            tr.addClass('shown');
        }

    });
    function format ( d ) {
        return '<table id="dataTable" class="table table-bordered" style="width: 100%">'+
            '<tr class="thead-dark">' +
            '<th>Előfizetés leírása</th>' +
            '</tr>'+
            '<td>'+d.membership_description+'</td>'+
            '</table>';
    };
    $(document).on('click','.btn-remove',function (data,event) {
        const $myId = $(this).data('id');
        $.ajax({
            method: 'POST',
            url : '/membership/remove/'+$myId,
            processData: false,
            contentType: false,
            destroy: true,
            success : function (obj) {
                if(obj.type=='success')
                {
                    alert('Sikeres felvitel!');
                    $('#metho').trigger('reset');
                }
                else alert('Sikertelen felvitel!');
            }
        });
    });
});