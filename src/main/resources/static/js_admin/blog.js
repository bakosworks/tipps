
$(document).ready(function(){
    var lang = {
        "sEmptyTable":     "Nincs rendelkezésre álló adat",
        "sInfo":           "Találatok: _START_ - _END_ Összesen: _TOTAL_",
        "sInfoEmpty":      "Nulla találat",
        "sInfoFiltered":   "(_MAX_ összes rekord közül szűrve)",
        "sInfoPostFix":    "",
        "sInfoThousands":  " ",
        "sLengthMenu":     "_MENU_ oldalanként",
        "sLoadingRecords": "Betöltés...",
        "sProcessing":     "Feldolgozás...",
        "sSearch":         "Keresés:",
        "sZeroRecords":    "Nincs a keresésnek megfelelő találat",
        "oPaginate": {
            "sFirst":    "Első",
            "sPrevious": "Előző",
            "sNext":     "Következő",
            "sLast":     "Utolsó"
        },
        "oAria": {
            "sSortAscending":  ": aktiválja a növekvő rendezéshez",
            "sSortDescending": ": aktiválja a csökkenő rendezéshez"
        }
    };
    var table = loadDT(table);
    $(document).on('click', "#btn_category_add", function() {
       if($("#DATA").valid())
       {
           var form = document.forms['DATA'];
           const formData = new FormData(form);
           if(form.checkValidity())
           {
               $.ajax({
                   method:'POST',
                   url: '/blog/add',
                   data: formData,
                   processData: false,
                   contentType: false,
                   success: function(data){
                       if(data.type=='success')
                       {
                           $('#myModal').modal('hide');
                           loadDT(table);
                       }
                       else if(data.type=='error')
                       {
                            alert('Sikertelen felvitel!');
                       }
                   },
                   error: function (error) {
                       alert(error);
                   }
               });
           }
       }

    });
    $('#btn_add_category').on('click',function (event) {
        $('#myModal').modal('show');
    });
    function loadDT(table_input){
        $.ajax({
            method: 'POST',
            url : '/blog/getAll',
            processData: false,
            contentType: false,
            destroy: true,
            success : function (obj) {
                if(table!=null || table!=undefined)
                {
                    table.destroy();
                }
                table_input = $('#dt_blogpost').DataTable({
                    data: obj,
                    dom: '<"wrapper"tp>',
                    language : lang,
                    pageLength : 10,
                    columns: [
                        {
                            data: 'post_id' },
                        {
                            data: 'post_title'
                        },
                        {
                            data: 'post_generated'
                        },
                        {
                            data:'post_id',
                            orderable:false,
                            searchable:false,
                            render : function (data) {
                                return  '<i class="fa fa-info-circle text-info fa-lg details-control btn btn-sm"></i>' +
                                    '<i class="ml-2 fa fa-trash btn btn-sm" id="btn_delete" data-id='+data+'></i>';
                            }
                        }
                    ]
                });
                table = table_input;
            }
        });
    };
    $(document).on('click', ".details-control", function() {
        var tr = $(this).closest('tr');
        var row = table.row( tr );

        if ( row.child.isShown() ) {
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            row.child( format(row.data()) ).show();
            tr.addClass('shown');
        }

    });
    function format ( d ) {
        return '<table id="dataTable" class="table table-bordered" style="width: 100%">'+
            '<tr class="thead-dark">' +
            '<th>Rövid leírás</th>' +
            '<th>Hosszú leírása</th>' +
            '</tr>'+
            '<td>'+d.short_description+'</td>'+
            '<td>'+d.post_description+'</td>'+
            '</table>';
    };
    $(document).on('click','#btn_delete',function (data) {
        const form =  new FormData();
        form.append('id',$(this).data('id'));
        $.ajax({
            method: 'POST',
            url : '/blog/delete',
            data: form,
            processData: false,
            contentType: false,
            destroy: true,
            success : function (data) {
                if(data.type=='success')
                {
                    alert('Sikeres');
                    loadDT(table);
                }
                else if(data.type=='error')
                {
                    alert('Sikertelen');
                }
            }
        });
    });

});