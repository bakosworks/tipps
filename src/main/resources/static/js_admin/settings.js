
$(document).ready(function(){

   $('#metho').submit(function (event) {
        var metho = document.forms['metho'];
        let $form = new FormData(metho);
        $.ajax({
            method: 'POST',
            data:$form,
            url : '/metho/add',
            processData: false,
            contentType: false,
            destroy: true,
            success : function (obj) {
                if(obj.type=='success')
                {
                    alert('Sikeres felvitel!');
                    $('#metho').trigger('reset');
                }
                else alert('Sikertelen felvitel!');
            }
        });
    });
    $('#staticForm').submit(function (event) {
        var staticForm = document.forms['staticForm'];
        let $form = new FormData(staticForm);
        $.ajax({
            method: 'POST',
            data:$form,
            url : '/admin-rest/static-edit',
            processData: false,
            contentType: false,
            success : function (obj) {
                if(obj.type=='success')
                {
                    alert('Sikeres felvitel!');
                }
                else alert('Sikertelen felvitel!');
            }
        });
    });

});