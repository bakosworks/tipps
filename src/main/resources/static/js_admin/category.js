
$(document).ready(function(){
    var lang = {
        "sEmptyTable":     "Nincs rendelkezésre álló adat",
        "sInfo":           "Találatok: _START_ - _END_ Összesen: _TOTAL_",
        "sInfoEmpty":      "Nulla találat",
        "sInfoFiltered":   "(_MAX_ összes rekord közül szűrve)",
        "sInfoPostFix":    "",
        "sInfoThousands":  " ",
        "sLengthMenu":     "_MENU_ oldalanként",
        "sLoadingRecords": "Betöltés...",
        "sProcessing":     "Feldolgozás...",
        "sSearch":         "Keresés:",
        "sZeroRecords":    "Nincs a keresésnek megfelelő találat",
        "oPaginate": {
            "sFirst":    "Első",
            "sPrevious": "Előző",
            "sNext":     "Következő",
            "sLast":     "Utolsó"
        },
        "oAria": {
            "sSortAscending":  ": aktiválja a növekvő rendezéshez",
            "sSortDescending": ": aktiválja a csökkenő rendezéshez"
        }
    };
    var table = loadDT(table);
    $(document).on('click', "#btn_category_add", function() {
       if($("#DATA").valid())
       {
           var form = document.forms['DATA'];
           const formData = new FormData(form);
           if(form.checkValidity())
           {
               $.ajax({
                   method:'POST',
                   url: '/category/add',
                   data: formData,
                   processData: false,
                   contentType: false,
                   success: function(data){
                       if(data.type=='success')
                       {
                           $('#myModal').modal('hide');
                           loadDT(table);
                       }
                       else if(data.type=='error')
                       {
                            alert('Sikertelen felvitel!');
                       }
                   },
                   error: function (error) {
                       alert(error);
                   }
               });
           }
       }

    });
    $('#btn_add_category').on('click',function (event) {
        $('#myModal').modal('show');
    });
    function loadDT(table_input){
        $.ajax({
            method: 'POST',
            url : '/category/getAll',
            processData: false,
            contentType: false,
            destroy: true,
            success : function (obj) {
                if(table!=null || table!=undefined)
                {
                    table.destroy();
                }
                table_input = $('#dt_category').DataTable({
                    data: obj,
                    dom: '<"wrapper"ftp>',
                    language : lang,
                    pageLength : 10,
                    columns: [
                        {
                            data: 'category_id' },
                        {
                            data: 'category_name'
                        },
                        {
                            data:'category_id',
                            orderable:false,
                            searchable:false,
                            render : function (data) {
                                return '<i class="fa fa-minus-circle" data-id='+data+'></i>';
                            }
                        }
                    ]
                });
                table = table_input;
            }
        });
    };

});