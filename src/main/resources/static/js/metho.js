$(document).ready(function(){

    $('.get-class').on('click',function () {
        $.ajax({
            method: 'POST',
            url : '/metho/get/'+$(this).data('id'),
            processData: false,
            contentType: false,
            destroy: true,
            success : function (obj) {
              $('#p_post').html(obj.object);
            }
        });
    });
    function loadDT(table_input){
        $.ajax({
            method: 'POST',
            url : '/metho/get/'+$(this).data('id'),
            processData: false,
            contentType: false,
            destroy: true,
            success : function (obj) {
                if(table!=null || table!=undefined)
                {
                    table.destroy();
                }
                table_input = $('#dt_tipps').DataTable({
                    data: obj,
                    dom: '<"wrapper"t>',
                    language : lang,
                    pageLength : 150,
                    createdRow: function( row , data) {
                            $(row).addClass( 'bg-true-white' );
                    },
                    columnDefs: [
                        {
                            targets: 1,
                            className: 'bg-dark'
                        }
                    ],
                    columns: [
                        {
                            data: 'tipp_id',
                            render:function(data,type,row)
                            {
                                let $player1 = row.player1;
                                let $player2 = row.player2;
                                let $type = row.hazaVvendeg;
                                let $odds = row.odds;

                                if($player1 == null)
                                {
                                    return $player2+'    '+$type+' '+$odds+'';
                                }
                                else if($player2 == null)
                                {
                                    return $player1+'    '+$type+' '+$odds+'';
                                }
                                return $player1+ ' - '+$player2+'    '+$type+' '+$odds+' </>';
                            }
                        },
                        {

                            data:'tipp_id',
                            orderable:false,
                            searchable:false,
                            render : function (data) {
                                return  '<span class="text-dark details-control btn btn-light btn-sm">Elemzés</span>'
                            }
                        }
                    ]
                });
                table = table_input;
            }
        });
    };
});