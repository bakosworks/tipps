$('#datetimepicker1').datetimepicker({
    format: 'YYYY-MM-DD HH:mm:ss'
});
$(document).ready(function(){
    var lang = {
        "sEmptyTable":     "Nincs rendelkezésre álló adat",
        "sInfo":           "Találatok: _START_ - _END_ Összesen: _TOTAL_",
        "sInfoEmpty":      "Nulla találat",
        "sInfoFiltered":   "(_MAX_ összes rekord közül szűrve)",
        "sInfoPostFix":    "",
        "sInfoThousands":  " ",
        "sLengthMenu":     "_MENU_ oldalanként",
        "sLoadingRecords": "Betöltés...",
        "sProcessing":     "Feldolgozás...",
        "sSearch":         "Keresés:",
        "sZeroRecords":    "Nincs a keresésnek megfelelő találat",
        "oPaginate": {
            "sFirst":    "Első",
            "sPrevious": "Előző",
            "sNext":     "Következő",
            "sLast":     "Utolsó"
        },
        "oAria": {
            "sSortAscending":  ": aktiválja a növekvő rendezéshez",
            "sSortDescending": ": aktiválja a csökkenő rendezéshez"
        }
    };
    var table = loadDT(table);
    var ClosedTable = loadDTClosed(ClosedTable);

    function loadDT(table_input){
        $.ajax({
            method: 'POST',
            url : '/tipp/getOpened',
            processData: false,
            contentType: false,
            destroy: true,
            success : function (obj) {
                if(table!=null || table!=undefined)
                {
                    table.destroy();
                }
                table_input = $('#dt_tipps').DataTable({
                    data: obj,
                    dom: '<"wrapper"t>',
                    language : lang,
                    responsive: true,
                    pageLength : 150,
                    createdRow: function( row , data) {
                            $(row).addClass( 'bg-true-white' );
                    },
                    columnDefs: [
                        {
                            targets: 1,
                            className: 'bg-dark'
                        }
                    ],
                    columns: [
                        {
                            data: 'tipp_id',
                            render:function(data,type,row)
                            {
                                let $player1 = row.player1;
                                let $player2 = row.player2;
                                let $type = row.hazaVvendeg;
                                let $odds = row.odds;

                                if($player1 == null)
                                {
                                    return $player2+'    '+$type+' '+$odds+'';
                                }
                                else if($player2 == null)
                                {
                                    return $player1+'    '+$type+' '+$odds+'';
                                }
                                return $player1+ ' - '+$player2+'    '+$type+' '+$odds+' </>';
                            }
                        },
                        {

                            data:'tipp_id',
                            orderable:false,
                            searchable:false,
                            render : function (data) {
                                return  '<span class="text-white details-control btn btn-green btn-sm">Elemzés</span>'
                            }
                        }
                    ]
                });
                table = table_input;
            }
        });
    };
    function loadDTClosed(table_input){
        $.ajax({
            method: 'POST',
            url : '/tipp/getClosed',
            processData: false,
            contentType: false,
            destroy: true,
            success : function (obj) {
                if(ClosedTable!=null || ClosedTable!=undefined)
                {
                    ClosedTable.destroy();
                }
                table_input = $('#dt_tipps_old').DataTable({
                    data: obj,
                    dom: '<"wrapper"t>',
                    language : lang,
                    responsive: true,
                    pageLength : 150,
                    createdRow: function( row , data) {
                        let $vOl = data.winOrLose;
                        if($vOl==1)
                        {
                            $(row).addClass('bg-success');
                        }
                        else if($vOl==2)
                        {
                            $(row).addClass('bg-danger');
                        }
                        else if($vOl==3)
                        {
                            $(row).addClass('bg-warning');
                        }

                    },
                    columnDefs: [
                        {
                            targets: 1,
                            className: 'bg-dark'
                        }
                    ],
                    columns: [
                        {
                            data: 'tipp_id',
                            render:function(data,type,row)
                            {
                                let $player1 = row.player1;
                                let $player2 = row.player2;
                                let $type = row.hazaVvendeg;
                                let $odds = row.odds;

                                if($player1 == null)
                                {
                                    return $player2+'    '+$type+' '+$odds+'';
                                }
                                else if($player2 == null)
                                {
                                    return $player1+'    '+$type+' '+$odds+'';
                                }
                                return $player1+ ' - '+$player2+'    '+$type+' '+$odds+' </>';
                            }
                        },
                        {

                            data:'tipp_id',
                            orderable:false,
                            searchable:false,
                            render : function (data) {
                                return  '<span class="text-white details-control-closed btn btn-green btn-sm">Elemzés</span>'
                            }
                        }
                    ]
                });
                ClosedTable = table_input;
            }
        });
    };
    $(document).on('click', ".details-control", function() {
        var tr = $(this).closest('tr');
        var row = table.row( tr );

        if ( row.child.isShown() ) {
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            row.child( format(row.data()) ).show();
            tr.addClass('shown');
        }
    });
    $(document).on('click', ".details-control-closed", function() {
        var tr = $(this).closest('tr');
        var row = ClosedTable.row( tr );

        if ( row.child.isShown() ) {
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            row.child( format(row.data()) ).show();
            tr.addClass('shown');
        }
    });
    function format ( d ) {
        return '<table id="dataTable" class="table table-bordered" style="width: 100%">'+
            '<tr class="thead-dark">' +
            '<th>Elemzés</th>' +
            '</tr>'+
            '<td><p class="post">'+d.description+'</p></td>'+
            '</table>';
    };
    $(document).on('click','#btn_delete',function (data) {
        const form =  new FormData();
        form.append('id',$(this).data('id'));
        $.ajax({
            method: 'POST',
            url : '/tipp/delete',
            data: form,
            processData: false,
            contentType: false,
            destroy: true,
            success : function (data) {
                if(data.type=='success')
                {
                    alert('Sikeres');
                    loadDT(table);
                }
                else if(data.type=='error')
                {
                    alert('Sikertelen');
                }
            }
        });
    });

});