
$(document).ready(function(){
    $('#pay').submit( function(event) {
            var form = document.forms['pay'];
            let $checked = $('#field_terms');
            let $zip = $('#inputZip').val().length;

        if($checked.prop("checked") == true && $zip==4){
                const formData = new FormData(form);
                formData.append("membership_name",$('#input_membership option:selected').text().trim());

                $.ajax({
                    method:'POST',
                    url: '/user/add',
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(data){
                        if(data.type=='success')
                        {
                            $('#span_reg_id').text(data.msg);
                            $('#modal_succes_reg').modal('show');
                            $('#pay').trigger('reset');
                        }
                        else if(data.type=='error')
                        {
                            alert('Server hiba!');
                        }
                    },
                    error: function (error) {
                        alert(error);
                    }
                });
        }
        else if($checked.prop("checked") == false){
        $('#p_aszf').html('<span class="alert alert-danger">Az ÁSzF elfogadás kötelező!</span>');
        }
        else if($zip>4 || $zip<4)
        {
            $('#span_zip').html('Az irányótszám 4 karakter hosszú kell hogy legyen!');
        }
            event.preventDefault();
    });
});