CREATE TABLE `role` (
                        `role_id` int(11) NOT NULL AUTO_INCREMENT,
                        `role` varchar(255) DEFAULT NULL,
                        PRIMARY KEY (`role_id`)
) ENGINE = MyISAM
  DEFAULT CHARACTER SET = utf8mb4;

CREATE TABLE  `table_category` (
                                  `category_id` int(11) NOT NULL AUTO_INCREMENT,
                                  `category_name` varchar(255) DEFAULT NULL,
                                  PRIMARY KEY (`category_id`)
) ENGINE = MyISAM
  DEFAULT CHARACTER SET = utf8mb4;

CREATE TABLE `table_blog_post` (
                                   `post_id` int(11) NOT NULL AUTO_INCREMENT,
                                   `post_description` varchar(255) DEFAULT NULL,
                                   `post_generated` datetime DEFAULT CURRENT_TIMESTAMP,
                                   `post_title` varchar(255) DEFAULT NULL,
                                   `short_description` varchar(255) DEFAULT NULL,
                                   `blog_category` int(11) DEFAULT NULL,
                                   PRIMARY KEY (`post_id`),
                                   KEY `FKq9xv5tdkjn60jttkyk33qgaiu` (`blog_category`),
                                   CONSTRAINT `FKq9xv5tdkjn60jttkyk33qgaiu` FOREIGN KEY (`blog_category`) REFERENCES `table_category` (`category_id`)
) ENGINE = MyISAM
  DEFAULT CHARACTER SET = utf8mb4;

CREATE TABLE `table_membership` (
                                    `membership_id` int(11) NOT NULL AUTO_INCREMENT,
                                    `generated_date` datetime DEFAULT NULL,
                                    `membership_description` varchar(255) DEFAULT NULL,
                                    `name` varchar(255) DEFAULT NULL,
                                    `price` double DEFAULT NULL,
                                    PRIMARY KEY (`membership_id`)
) ENGINE = MyISAM
  DEFAULT CHARACTER SET = utf8mb4;

CREATE TABLE `table_tipp` (
                              `tipp_id` int(11) NOT NULL AUTO_INCREMENT,
                              `player1` varchar(255) DEFAULT NULL,
                              `player2` varchar(255) DEFAULT NULL,
                              `description` varchar(255) DEFAULT NULL,
                              `is_premium` bit(1) DEFAULT NULL,
                              `short_description` varchar(255) DEFAULT NULL,
                              `tipp_generated` datetime DEFAULT CURRENT_TIMESTAMP,
                              `title` varchar(255) DEFAULT NULL,
                              `category` int(11) DEFAULT NULL,
                              `win_or_lose` int(30) DEFAULT NULL,
                              `odds` double DEFAULT NULL,
                              `haza_v_vendeg` varchar(20) DEFAULT NULL,
                              PRIMARY KEY (`tipp_id`),
                              KEY `FK38f1x7twtsk1b06btsx7wkid7` (`category`),
                              CONSTRAINT `FK38f1x7twtsk1b06btsx7wkid7` FOREIGN KEY (`category`) REFERENCES `table_category` (`category_id`)
)ENGINE = MyISAM
 DEFAULT CHARACTER SET = utf8mb4;

CREATE TABLE  `user` (
                        `user_id` int(11) NOT NULL AUTO_INCREMENT,
                        `active` int(11) DEFAULT NULL,
                        `email` varchar(255) NOT NULL,
                        `password` varchar(255) NOT NULL,
                        `username` varchar(255) NOT NULL,
                        `membership` varchar(255) DEFAULT NULL,
                        `pay_id` varchar(255) DEFAULT NULL,
                        `full_name` varchar(255) DEFAULT NULL,
                        `user_created` datetime DEFAULT CURRENT_TIMESTAMP,
                        PRIMARY KEY (`user_id`)
) ENGINE = MyISAM
  DEFAULT CHARACTER SET = utf8mb4;

CREATE TABLE `user_role` (
                             `user_id` int(11) NOT NULL,
                             `role_id` int(11) NOT NULL,
                             PRIMARY KEY (`user_id`,`role_id`),
                             KEY `FKa68196081fvovjhkek5m97n3y` (`role_id`),
                             CONSTRAINT `FK859n2jvi8ivhui0rl0esws6o` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`),
                             CONSTRAINT `FKa68196081fvovjhkek5m97n3y` FOREIGN KEY (`role_id`) REFERENCES `role` (`role_id`)
)ENGINE = MyISAM
 DEFAULT CHARACTER SET = utf8mb4;
CREATE TABLE `table_static` (
                                        `static_id` INT NOT NULL AUTO_INCREMENT,
                                        `player` VARCHAR(45) NULL,
                                        `player2` VARCHAR(45) NULL,
                                        `wind` VARCHAR(45) NULL,
                                        `wind2` VARCHAR(45) NULL,
                                        `pback` VARCHAR(45) NULL,
                                        `pback2` VARCHAR(45) NULL,
                                        `losd` VARCHAR(45) NULL,
                                        `losd2` VARCHAR(45) NULL,
                                        `avg` VARCHAR(45) NULL,
                                        `avg2` VARCHAR(45) NULL,
                                        `profit` VARCHAR(45) NULL,
                                        `profit2` VARCHAR(45) NULL,
                                        `extra` VARCHAR(45) NULL,
                                        `extra2` VARCHAR(45) NULL,
                                        PRIMARY KEY (`static_id`))ENGINE = MyISAM
                                                                  DEFAULT CHARACTER SET = utf8mb4;

