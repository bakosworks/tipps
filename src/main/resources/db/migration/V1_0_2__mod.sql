ALTER TABLE `table_tipp`
    CHANGE COLUMN `short_description` `short_description` LONGTEXT NULL DEFAULT NULL ;
ALTER TABLE `table_tipp`
    CHANGE COLUMN `description` `description` LONGTEXT NULL DEFAULT NULL ;
ALTER TABLE `user`
    ADD COLUMN `address` VARCHAR(255) NULL AFTER `user_created`;