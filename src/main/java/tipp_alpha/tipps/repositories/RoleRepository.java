package tipp_alpha.tipps.repositories;


import org.springframework.data.jpa.repository.JpaRepository;
import tipp_alpha.tipps.models.Role;

public interface RoleRepository extends JpaRepository<Role, Integer> {
    Role findByRole(String role);

}