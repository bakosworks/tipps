package tipp_alpha.tipps.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import tipp_alpha.tipps.DTOS.MethoButtonsDTO;
import tipp_alpha.tipps.models.BlogPost;
import tipp_alpha.tipps.models.Metho;

import java.util.List;

public interface MethoRepository extends JpaRepository<Metho, Integer>, CrudRepository<Metho,Integer>
{

    @Query(nativeQuery = true,value = "SELECT * FROM table_metho where metho_id = 1")
    Metho getFirst();

    @Query(nativeQuery = true, value = "SELECT metho_id,button_name FROM table_metho")
    List<Metho> getMethos();


}