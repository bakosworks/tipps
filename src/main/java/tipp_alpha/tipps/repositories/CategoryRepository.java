package tipp_alpha.tipps.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import tipp_alpha.tipps.models.Category;

public interface CategoryRepository extends JpaRepository<Category, Integer>, CrudRepository<Category,Integer>
{

}