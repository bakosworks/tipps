package tipp_alpha.tipps.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import tipp_alpha.tipps.models.User;

public interface UserRepository extends JpaRepository<User, Integer> {
    User findByUsername(String username);
}