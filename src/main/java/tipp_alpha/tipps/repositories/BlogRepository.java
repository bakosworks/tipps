package tipp_alpha.tipps.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import tipp_alpha.tipps.models.BlogPost;
import tipp_alpha.tipps.models.Tipp;

import java.util.List;

public interface BlogRepository extends JpaRepository<BlogPost, Integer>, CrudRepository<BlogPost,Integer>
{

    @Query(nativeQuery = true,value = "SELECT * FROM table_blog_post \n" +
            "order by post_generated desc\n" +
            "limit 9")
    List<BlogPost> getLast9BlogPost();

}