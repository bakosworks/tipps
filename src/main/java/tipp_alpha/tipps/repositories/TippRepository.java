package tipp_alpha.tipps.repositories;

import org.joda.time.DateTime;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import tipp_alpha.tipps.models.Category;
import tipp_alpha.tipps.models.Tipp;

import java.util.List;

public interface TippRepository extends JpaRepository<Tipp, Integer>, CrudRepository<Tipp,Integer>
{

    @Query(nativeQuery = true,value = "SELECT * FROM table_tipp where is_premium = 0 and win_or_lose = 0\n" +
            "order by tipp_generated desc\n" +
            "limit 9")
    List<Tipp> getFreeTippsToday();

    @Query(nativeQuery = true,value = "SELECT * FROM table_tipp \n" +
            "where DATE(tipp_generated) = DATE(:dateTime)\n" +
            "and is_premium = 1\n" +
            "order by tipp_generated desc\n" +
            "limit 9")
    List<Tipp> getPremiumTippsToday(@Param("dateTime")String dateTime);

    @Query(nativeQuery = true,value = "SELECT * FROM table_tipp where win_or_lose != 0 and is_premium=1")
    List<Tipp> getClosedTipps();

    @Query(nativeQuery = true,value = "SELECT * FROM table_tipp where win_or_lose = 0 and is_premium=1 order by tipp_generated desc")
    List<Tipp> getOpenedTipps();

    @Query(nativeQuery = true,value = "SELECT * FROM table_tipp where win_or_lose != 0")
    List<Tipp> getClosedTippsforAdmin();

    @Query(nativeQuery = true,value = "SELECT * FROM table_tipp where win_or_lose = 0 order by tipp_generated desc")
    List<Tipp> getOpenedTippsforAdmin();


}