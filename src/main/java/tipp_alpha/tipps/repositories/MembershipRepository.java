package tipp_alpha.tipps.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import tipp_alpha.tipps.models.Membership;

public interface MembershipRepository extends JpaRepository<Membership, Integer>, CrudRepository<Membership,Integer>
{

}