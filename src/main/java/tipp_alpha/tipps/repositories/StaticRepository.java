package tipp_alpha.tipps.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import tipp_alpha.tipps.models.Statics;

public interface StaticRepository extends JpaRepository<Statics, Integer>, CrudRepository<Statics,Integer>
{
}