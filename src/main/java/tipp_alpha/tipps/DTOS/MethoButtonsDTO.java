package tipp_alpha.tipps.DTOS;

import lombok.Data;

import java.security.Timestamp;

@Data
public class MethoButtonsDTO {

    private int metho_id;

    private String button_name;
}
