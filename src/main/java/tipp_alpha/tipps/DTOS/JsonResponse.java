package tipp_alpha.tipps.DTOS;

import java.util.List;
import java.util.Map;

/**
 * Created by rayer on 2017.10.03..
 */
public class JsonResponse {

    private String msg;

    private String type;

    private Map<String,String> extra;

    private List<?> extraList;

    private Object object;

    public Object getObject() {
        return object;
    }

    public void setObject(Object object) {
        this.object = object;
    }

    public JsonResponse(String msg, String type, Map<String, String> extra){
        this.msg = msg;
        this.type = type;
        this.extra = extra;
    }

    public JsonResponse(String msg, String type, Map<String, String> extra, List<?> extraList){
        this.msg = msg;
        this.type = type;
        this.extra = extra;
        this.extraList = extraList;
    }

    public JsonResponse(String msg, String type){
        this.msg = msg;
        this.type = type;
    }
    public JsonResponse(String msg, String type, Object object){
        this.msg = msg;
        this.type = type;
        this.object = object;
    }

    public JsonResponse() {
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Map<String, String> getExtra() {
        return extra;
    }

    public void setExtra(Map<String, String> extra) {
        this.extra = extra;
    }

    public List<?> getExtraList() {
        return extraList;
    }

    public void setExtraList(List<?> extraList) {
        this.extraList = extraList;
    }
}
