package tipp_alpha.tipps.DTOS;

import lombok.Data;
import org.joda.time.DateTime;

import java.security.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Data
public class BlogPostDTO {

    private int post_id;

    private String post_title;

    private String post_description;

    private String short_description;

    private String blog_category;

    private Timestamp post_generated;



}
