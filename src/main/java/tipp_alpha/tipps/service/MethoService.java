package tipp_alpha.tipps.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tipp_alpha.tipps.DTOS.MethoButtonsDTO;
import tipp_alpha.tipps.models.Metho;
import tipp_alpha.tipps.repositories.MethoRepository;

import java.util.List;


@Service
public class MethoService {

    @Autowired
    MethoRepository methoRepository;

   public Metho  getFirstMetho(){
       return methoRepository.getFirst();
   }
   public List<Metho> getMethos()
   {
       return methoRepository.findAll();
   }
   public Metho getMethoById(Integer Id)
   {
       return methoRepository.getOne(Id);
   }
   public void saveMetho(Metho metho)
   {
       methoRepository.save(metho);
   }
}