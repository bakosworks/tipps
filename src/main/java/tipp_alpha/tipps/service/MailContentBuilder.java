package tipp_alpha.tipps.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

@Service
public class MailContentBuilder {

    private TemplateEngine templateEngine;

    @Autowired
    public MailContentBuilder(TemplateEngine templateEngine) {
        this.templateEngine = templateEngine;
    }

    public String build(String message,String pay_id) {
        Context context = new Context();
        context.setVariable("message", message);
        context.setVariable("pay_id", pay_id);
        return templateEngine.process("/mail/email", context);
    }

}