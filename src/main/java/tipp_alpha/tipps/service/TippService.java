package tipp_alpha.tipps.service;


import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tipp_alpha.tipps.models.Category;
import tipp_alpha.tipps.models.Tipp;
import tipp_alpha.tipps.repositories.CategoryRepository;
import tipp_alpha.tipps.repositories.TippRepository;

import java.util.List;


@Service
public class TippService {

    @Autowired
    TippRepository tippRepository;

    public void save(Tipp tipp) {
        tippRepository.save(tipp);
    }
    public boolean saveBoolean(Tipp tipp) {
        boolean yes = false;
        try {
            tippRepository.save(tipp);
            return yes = true;
        }
        catch (Exception e)
        {
            return yes;
        }
    }
    public List<Tipp> findAll() {
        return tippRepository.findAll();
    }

    public List<Tipp> getFreeTippsToday(){
        return tippRepository.getFreeTippsToday();
    }

    public List<Tipp> getPremiumTippsToday(){
        DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
        DateTime dateTime = DateTime.now();
        String DT = dateTime.toString(fmt);

        return tippRepository.getPremiumTippsToday(DT);
    }
    public void deleteById(Integer id)
    {
        tippRepository.delete(id);
    }

    public List<Tipp> getClosedTipps() {
     return   tippRepository.getClosedTipps();
    }

    public List<Tipp> getOpenedTipps() {
      return  tippRepository.getOpenedTipps();
    }
    public Tipp findOneById(Integer tippId)
    {
        return tippRepository.findOne(tippId);
    }

    public List<Tipp> getClosedTippsforAdmin() {
        return   tippRepository.getClosedTippsforAdmin();
    }

    public List<Tipp> getOpenedTippsforAdmin() {
        return  tippRepository.getOpenedTippsforAdmin();
    }
}