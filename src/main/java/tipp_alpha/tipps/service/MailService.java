package tipp_alpha.tipps.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tipp_alpha.tipps.models.BlogPost;
import tipp_alpha.tipps.repositories.BlogRepository;

import javax.mail.*;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Properties;


@Service
public class MailService {

    @Autowired
    MailContentBuilder mailContentBuilder;

    public void sendmail(String message,String pay_id,String emailOwner) throws MessagingException {
        Properties props = new Properties();
        //props.put("mail.smtp.auth", "true");
        //props.put("mail.smtp.starttls.enable", "true");
        //props.put("mail.smtp.host", "mail.bakosworks.com");
        //props.put("mail.smtp.port", "8025");

        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "mail.msbsport.info");
        props.put("mail.smtp.port", "8025");


        Session session = Session.getInstance(props, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                //return new PasswordAuthentication("test@bakosworks.com", "GyereRamViki@");
                return new PasswordAuthentication("noreply@msbsport.info", "zGSrcaGtHi_Q");
            }
        });

        Message msg = new MimeMessage(session);
        //msg.setFrom(new InternetAddress("test@bakosworks.com", false));
        msg.setFrom(new InternetAddress("noreply@msbsport.info", false));

        String content = mailContentBuilder.build(message,pay_id);
        String sendFor = emailOwner+",archive@msbsport.info";
        //String sendFor = emailOwner+",archive@bakosworks.com";
        msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(sendFor));
        msg.setSubject("Csomag igénylés");
        msg.setContent(content, "text/html; charset=utf-8");
        msg.setSentDate(new Date());

        //MimeBodyPart messageBodyPart = new MimeBodyPart();
        //messageBodyPart.setContent("Content", "UTF-8");
        //Multipart multipart = new MimeMultipart();
        //multipart.addBodyPart(messageBodyPart);
        //MimeBodyPart attachPart = new MimeBodyPart();
        //attachPart.attachFile("/var/tmp/image19.png");
        //multipart.addBodyPart(attachPart);
        //msg.setContent(multipart);
        Transport.send(msg);
    }
}