package tipp_alpha.tipps.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tipp_alpha.tipps.models.Membership;
import tipp_alpha.tipps.repositories.MembershipRepository;

import java.util.List;


@Service
public class MembershipService {

    @Autowired
    MembershipRepository membershipRepositroy;

    public void save(Membership membership) {
        membershipRepositroy.save(membership);
    }

    public List<Membership> findAll() {
        return membershipRepositroy.findAll();
    }

    public void deleteById(Integer id)
    {
        membershipRepositroy.delete(id);
    }
}