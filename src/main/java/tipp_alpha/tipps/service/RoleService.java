package tipp_alpha.tipps.service;


import com.fasterxml.jackson.annotation.JacksonAnnotationsInside;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import tipp_alpha.tipps.models.Role;
import tipp_alpha.tipps.models.User;
import tipp_alpha.tipps.repositories.RoleRepository;
import tipp_alpha.tipps.repositories.UserRepository;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

@Service
public class RoleService {

    @Autowired
    RoleRepository roleRepository;

    public List<Role> getAllRole()
    {
       return roleRepository.findAll();
    }

}