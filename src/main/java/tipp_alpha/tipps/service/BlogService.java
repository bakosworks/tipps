package tipp_alpha.tipps.service;


import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tipp_alpha.tipps.models.BlogPost;
import tipp_alpha.tipps.models.Tipp;
import tipp_alpha.tipps.repositories.BlogRepository;
import tipp_alpha.tipps.repositories.TippRepository;

import java.util.List;


@Service
public class BlogService {

    @Autowired
    BlogRepository blogRepository;

    public void save(BlogPost blogPost) {
        blogRepository.save(blogPost);
    }
    public List<BlogPost> findAll()
    {
        return blogRepository.findAll();
    }

    public List<BlogPost> getLast9BlostPost(){
        return blogRepository.getLast9BlogPost();
    }

    public void  deleteById(Integer id)
    {
          blogRepository.delete(id);
    }

    public BlogPost getById(Integer Id)
    {
        return blogRepository.getOne(Id);
    }
}