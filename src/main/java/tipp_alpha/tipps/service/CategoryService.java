package tipp_alpha.tipps.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tipp_alpha.tipps.models.Category;
import tipp_alpha.tipps.repositories.CategoryRepository;

import java.util.List;


@Service
public class CategoryService {

    @Autowired
    CategoryRepository categoryRepository;

    public void save(String name) {
        Category c = new Category();
        c.setCategory_name(name);
        categoryRepository.save(c);
    }

    public List<Category> findAll() {
        return categoryRepository.findAll();
    }
}