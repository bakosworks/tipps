package tipp_alpha.tipps.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tipp_alpha.tipps.models.BlogPost;
import tipp_alpha.tipps.models.Statics;
import tipp_alpha.tipps.repositories.BlogRepository;
import tipp_alpha.tipps.repositories.StaticRepository;

import java.util.List;


@Service
public class StaticService {

    @Autowired
    StaticRepository staticRepository;

  public List<Statics> getAll()
  {
        return  staticRepository.findAll();
  }

  public boolean save(Statics statics)
  {
      try {
          staticRepository.save(statics);
          return true;
      }catch (Exception e)
      {
         return false;
      }
  }
}