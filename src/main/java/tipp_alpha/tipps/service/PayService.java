package tipp_alpha.tipps.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tipp_alpha.tipps.models.BlogPost;
import tipp_alpha.tipps.repositories.BlogRepository;

import java.util.List;


@Service
public class PayService {

   public String generatedPayID(Integer length){
       String result= "";
       String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
       Integer charactersLength = characters.length();
       for ( int i = 0; i < length; i++ ) {
           double d = Math.floor(Math.random() * charactersLength.doubleValue());
           result += characters.charAt((int)d);
       }
       return result;
   }
}