package tipp_alpha.tipps.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;

@Table(name = "table_blog_post")
@Entity
@Data
public class BlogPost {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "post_id")
    private int post_id;

    @Column(name="post_title")
    private String post_title;

    @Column(name="post_description")
    private String post_description;

    @Column(name="short_description")
    private String short_description;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="blog_category")
    private Category blog_category;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss" , timezone = "Europe/London")
    @Column(name="post_generated")
    private Timestamp post_generated;
}
