package tipp_alpha.tipps.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.*;
import javax.xml.soap.Text;
import java.sql.Timestamp;

@Table(name = "table_metho")
@Entity
@Data
public class Metho {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "metho_id")
    private int metho_id;

    @Column(name="button_name")
    private String button_name;

    @Lob
    @Column(name="full_post",length = 500)
    private String full_post;
}
