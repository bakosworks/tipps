package tipp_alpha.tipps.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;

@Table(name = "table_tipp")
@Entity
@Data
public class Tipp {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "tipp_id")
    private int tipp_id;

    @Column(name="title")
    private String title;

    @Column(name="Player1")
    private String Player1;

    @Column(name="Player2")
    private String Player2;

    @Lob
    @Column(name="description")
    private String description;

    @Lob
    @Column(name="short_description")
    private String short_description;

    @Column(name="isPremium")
    private boolean isPremium;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="category")
    private Category category;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern =  "yyyy-MM-dd HH:mm:ss")
    @Column(name="tipp_generated")
    private Timestamp tipp_generated;

    @Column(name = "win_or_lose")
    private Integer winOrLose;

    @Column(name = "odds")
    private double odds;

    @Column(name="haza_v_vendeg")
    private String hazaVvendeg;
}
