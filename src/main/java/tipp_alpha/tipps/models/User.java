package tipp_alpha.tipps.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Set;

@Data
@Entity
@Table(name = "user")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "user_id")
    private int id;
    @Column(name = "email")
    @Email(message = "*Please provide a valid Email")
    @NotEmpty(message = "*Please provide an email")
    private String email;
    @Column(name = "password")
    @Length(min = 5, message = "*Your password must have at least 5 characters")
    @NotEmpty(message = "*Please provide your password")
    private String password;
    @Column(name = "username")
    @NotEmpty(message = "*Please provide your name")
    private String username;
    @Column(name = "active")
    private int active;
    @ManyToMany(cascade = CascadeType.PERSIST)
    @JoinTable(name = "user_role", joinColumns = @JoinColumn(name = "user_id"), inverseJoinColumns = @JoinColumn(name = "role_id"))
    private Set<Role> roles;

    @Column(name = "membership")
    private String membership;

    @Column(name = "pay_id")
    private String pay_id;

    @Column(name = "full_name")
    private String full_name;

    @JsonFormat(pattern = "yyyy.MM.dd HH:mm:ss", timezone = "Europe/Budapest")
    @Column(name = "user_created")
    private Timestamp user_created;

    @Column(name = "address")
    private String address;
}