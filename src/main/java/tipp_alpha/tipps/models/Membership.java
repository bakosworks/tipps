package tipp_alpha.tipps.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Table(name = "table_membership")
@Entity
@Data
public class Membership {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "membership_id")
    private int membership_id;

    @Column(name="name")
    private String name;

    @Column(name="price")
    private double price;

    @Column(name="membership_description")
    private String membership_description;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm")
    @Column(name="generated_date")
    private Date generated_date;

}
