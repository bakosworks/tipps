package tipp_alpha.tipps.models;

import lombok.Data;

import javax.persistence.*;

@Table(name = "table_static")
@Entity
@Data
public class Statics {

    @Id
    @Column(name="static_id")
    private Integer staticId;

    @Column(name="player")
    private String player;

    @Column(name="player2")
    private String player2;

    @Column(name="wind")
    private String wind;

    @Column(name="wind2")
    private String wind2;

    @Column(name="pback")
    private String pback;

    @Column(name="pback2")
    private String pback2;

    @Column(name="losd")
    private String losd;

    @Column(name="losd2")
    private String losd2;

    @Column(name="avg")
    private String avg;

    @Column(name="avg2")
    private String avg2;

    @Column(name="profit")
    private String profit;

    @Column(name="profit2")
    private String profit2;

    @Column(name="extra")
    private String extra;

    @Column(name="extra2")
    private String extra2;

}
