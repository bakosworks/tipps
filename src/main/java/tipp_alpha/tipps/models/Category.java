package tipp_alpha.tipps.models;

import lombok.Data;

import javax.persistence.*;

@Table(name = "table_category")
@Entity
@Data
public class Category {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="category_id")
    private int category_id;

    @Column(name="category_name")
    private String category_name;
}
