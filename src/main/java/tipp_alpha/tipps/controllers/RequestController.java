package tipp_alpha.tipps.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import tipp_alpha.tipps.service.UserService;

@Controller
@RequestMapping(path = "/request", method = RequestMethod.GET)
public class RequestController {

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/index", method =RequestMethod.GET)
    public String index(Model model) {
        return "request/index";

    }


}
