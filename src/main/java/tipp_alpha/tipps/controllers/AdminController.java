package tipp_alpha.tipps.controllers;

import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import tipp_alpha.tipps.models.Role;
import tipp_alpha.tipps.models.Statics;
import tipp_alpha.tipps.service.RoleService;
import tipp_alpha.tipps.service.StaticService;

import java.util.List;

@Data
@Controller
@RequestMapping(path = "/admin", method = RequestMethod.GET)
public class AdminController {

    @Autowired
    RoleService roleService;

    @Autowired
    StaticService staticService;

    @RequestMapping(value = {"/index"}, method =RequestMethod.GET)
    public String index(Model model) {
        return "admin/index";

    }
    @RequestMapping(value = {"/tipp"}, method =RequestMethod.GET)
    public String tipp(Model model) {
        return "admin/tipp_control";

    }
    @RequestMapping(value = {"/category"}, method =RequestMethod.GET)
    public String category(Model model) {
        return "admin/category";

    }
    @RequestMapping(value = {"/blog"}, method =RequestMethod.GET)
    public String blog(Model model) {
        return "admin/blog";

    }
    @RequestMapping(value = {"/membership"}, method =RequestMethod.GET)
    public String membership(Model model) {
        return "admin/membership";

    }
    @RequestMapping(value = {"/user"}, method =RequestMethod.GET)
    public String user(Model model) {
        List<Role> roles =  roleService.getAllRole();
        model.addAttribute("roles",roles);
        return "admin/user";

    }
    @RequestMapping(value = {"/settings"}, method =RequestMethod.GET)
    public String settings(Model model) {
        List<Statics> statics = staticService.getAll();
        model.addAttribute("staticList",statics);
        return "admin/admin_settings";

    }


}
