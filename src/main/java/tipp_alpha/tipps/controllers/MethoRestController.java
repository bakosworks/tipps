package tipp_alpha.tipps.controllers;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import tipp_alpha.tipps.DTOS.JsonResponse;
import tipp_alpha.tipps.models.Membership;
import tipp_alpha.tipps.models.Metho;
import tipp_alpha.tipps.service.MembershipService;
import tipp_alpha.tipps.service.MethoService;

import java.util.Date;
import java.util.List;

@RequestMapping( value = "/metho")
@RestController
public class MethoRestController {

    @Autowired
    MethoService methoService;

    @RequestMapping(value = {"/get/{id}"}, method =RequestMethod.POST)
    public JsonResponse getMetho(@PathVariable("id") String id) {
        Metho metho = methoService.getMethoById(Integer.parseInt(id));
        return new JsonResponse("msg","success",metho.getFull_post());
    }
    @RequestMapping(value = {"/add"}, method =RequestMethod.POST)
    public JsonResponse addMetho(@RequestParam String button,@RequestParam String post) {
        Metho metho = new Metho();
        metho.setButton_name(button);
        metho.setFull_post(post);
        try {
            methoService.saveMetho(metho);
            return new JsonResponse("msg","success");
        }catch (Exception e)
        {
            return new JsonResponse("msg","error");
        }
    }
}
