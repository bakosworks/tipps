package tipp_alpha.tipps.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import tipp_alpha.tipps.controllers.admin_rest.BlogRestController;
import tipp_alpha.tipps.models.BlogPost;
import tipp_alpha.tipps.service.BlogService;
import tipp_alpha.tipps.service.UserService;

import java.util.List;

@Controller
public class IndexController {


    @Autowired
    private UserService userService;

    @Autowired
    BlogService blogService;

    @RequestMapping(value = {"/index","/"}, method =RequestMethod.GET)
    public String index(Model model) {
        List<BlogPost> blogPosts = blogService.getLast9BlostPost();
        model.addAttribute("blogPosts",blogPosts);
            return "index";

    }


}
