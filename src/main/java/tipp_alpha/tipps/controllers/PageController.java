package tipp_alpha.tipps.controllers;



import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import tipp_alpha.tipps.DTOS.MethoButtonsDTO;
import tipp_alpha.tipps.models.*;
import tipp_alpha.tipps.repositories.MembershipRepository;
import tipp_alpha.tipps.service.*;

import java.security.Principal;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@RequestMapping( value = "/page",method = RequestMethod.GET)
@Controller
public class PageController {

    @Autowired
    AdminController adminController;

    @Autowired
    LoginController loginController;

    @Autowired
    MembershipRepository membershipRepository;

    @Autowired
    TippService tippService;

    @Autowired
    BlogService blogService;

    @Autowired
    MembershipService membershipService;

    @Autowired
    StaticService staticService;

    @Autowired
    MethoService methoService;

    @Autowired
    ModelMapper modelMapper;

    @RequestMapping(value = {"/membership"}, method =RequestMethod.GET)
    public String membership(Model model) {
        List<Membership> memberships = membershipRepository.findAll();
        model.addAttribute("membershipList",memberships);
        return "pages/prepay";

    }
    @RequestMapping(value = {"/freetipps"}, method =RequestMethod.GET)
    public String freeTipps(Model model) {
        List<Tipp> tipps = tippService.getFreeTippsToday();
        model.addAttribute("freeTipps",tipps);
        return "pages/free_tipp";

    }
    @RequestMapping(value = {"/post/{id}"}, method =RequestMethod.GET)
    public String readPost(Model model,@PathVariable Integer id) {
        BlogPost blogPost = blogService.getById(id);
        model.addAttribute("post",blogPost);
        return "pages/post_read";

    }
    @RequestMapping(value = {"/premiumtipps"}, method =RequestMethod.GET)
    public String prepiumTipps(Model model, Principal principal) {
        Collection<?> auth = SecurityContextHolder.getContext().getAuthentication().getAuthorities();

        if(auth.contains(new SimpleGrantedAuthority("ADMIN")) || auth.contains(new SimpleGrantedAuthority("USER")))
        {
            return "pages/premium_tipp";
        }
        else
        {
            return "login";
        }
        //ROLE_ANONYMOUS
    }
    @RequestMapping(value = {"/methodology"}, method =RequestMethod.GET)
    public String methodology(Model model) {
        Metho metho = methoService.getFirstMetho();
        List<Metho> methos = methoService.getMethos();
        List<MethoButtonsDTO> methoButtonsDTOS =  methos.stream().map(x->modelMapper.map(x,MethoButtonsDTO.class)).collect(Collectors.toList());
        model.addAttribute("buttons",methos);
        model.addAttribute("fistMetho",metho);
        return "pages/methodology";

    }
    @RequestMapping(value = {"/statics"}, method =RequestMethod.GET)
    public String statics(Model model) {
        List<Statics> statics = staticService.getAll();
        model.addAttribute("staticList",statics);
        return "pages/statics";

    }
    @RequestMapping(value = {"/free/{id}"}, method =RequestMethod.GET)
    public String freeRead(Model model,@PathVariable Integer id) {
        Tipp freeTipp = tippService.findOneById(id);
        model.addAttribute("tipp",freeTipp);
        return "pages/free_tipps_read";
    }
    @RequestMapping(value = {"/contact"}, method =RequestMethod.GET)
    public String contact(Model model) {
        return "pages/cont";

    }
    @RequestMapping(value = {"/index"}, method =RequestMethod.GET)
    public String index(Model model) {
        List<BlogPost> blogPosts = blogService.getLast9BlostPost();
        model.addAttribute("blogPosts",blogPosts);
        return "index";

    }
    @RequestMapping(value = {"/admin"}, method =RequestMethod.GET)
    public String admin(Model model) {
        return adminController.index(model);
    }

    @RequestMapping(value = {"/login"}, method =RequestMethod.GET)
    public ModelAndView login(Model model) {
        return new ModelAndView("login");
    }

    @RequestMapping(value = {"/pay/{id}"}, method =RequestMethod.GET)
    public String pay(@PathVariable Integer id,Model model) {
        List<Membership> memberships = membershipService.findAll();
        model.addAttribute("memberships",memberships);
        return  "pages/payin";
    }
}
