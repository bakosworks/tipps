package tipp_alpha.tipps.controllers.admin_rest;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import tipp_alpha.tipps.DTOS.JsonResponse;
import tipp_alpha.tipps.models.Membership;
import tipp_alpha.tipps.service.MembershipService;

import java.util.Date;
import java.util.List;

@RequestMapping( value = "/membership")
@RestController
public class MembershipRestController {

    @Autowired
    MembershipService membershipService;

    @RequestMapping(value = {"/add"}, method =RequestMethod.POST)
    public JsonResponse addMembership(@RequestParam String input_name,
                                      @RequestParam String input_description,
                                      @RequestParam Integer input_price) {
        Membership membership = new Membership();
        membership.setMembership_description(input_description);
        membership.setPrice(input_price);
        membership.setName(input_name);
        membership.setGenerated_date(new Date());
        try {
            membershipService.save(membership);
            return new JsonResponse("","success");
        }
        catch (Exception e)
        {
            return new JsonResponse(e.getMessage(),"success");
        }
    }
    @RequestMapping(value = {"/getAll"}, method =RequestMethod.POST)
    public List<Membership> getAllMembership() {
        return membershipService.findAll();
    }

    @PostMapping(value = "/remove/{id}")
    public JsonResponse removeMembership(@PathVariable String id)
    {
        try {
            membershipService.deleteById(Integer.parseInt(id));
            return new JsonResponse("sikeres","success");
        }catch (Exception e)
        {
            return new JsonResponse("sikertelen","error");
        }
    }
}
