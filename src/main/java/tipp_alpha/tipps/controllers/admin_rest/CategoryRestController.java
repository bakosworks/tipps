package tipp_alpha.tipps.controllers.admin_rest;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import tipp_alpha.tipps.DTOS.JsonResponse;
import tipp_alpha.tipps.models.Category;
import tipp_alpha.tipps.service.CategoryService;

import java.util.List;

@RequestMapping( value = "/category")
@RestController
public class CategoryRestController {

    @Autowired
    CategoryService categoryService;

    @RequestMapping(value = {"/add"}, method =RequestMethod.POST)
    public JsonResponse addCategory(@RequestParam String input_name) {
        try {
            categoryService.save(input_name);
            return new JsonResponse("","success");
        }
        catch (Exception e)
        {
            return new JsonResponse(e.getMessage(),"success");
        }
    }
    @RequestMapping(value = {"/getAll"}, method =RequestMethod.POST)
    public List<Category> getAllCategory() {
        return categoryService.findAll();
    }

}
