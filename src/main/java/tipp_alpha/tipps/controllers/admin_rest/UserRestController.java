package tipp_alpha.tipps.controllers.admin_rest;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailSender;
import org.springframework.web.bind.annotation.*;
import tipp_alpha.tipps.DTOS.JsonResponse;
import tipp_alpha.tipps.controllers.MailRestController;
import tipp_alpha.tipps.models.Tipp;
import tipp_alpha.tipps.models.User;
import tipp_alpha.tipps.service.MailService;
import tipp_alpha.tipps.service.PayService;
import tipp_alpha.tipps.service.TippService;
import tipp_alpha.tipps.service.UserService;

import javax.mail.MessagingException;
import java.sql.Timestamp;
import java.util.List;

@RequestMapping( value = "/user")
@RestController
public class UserRestController {

    @Autowired
    UserService userService;

    @Autowired
    PayService payService;

    @Autowired
    MailService mailService;

    @RequestMapping(value = {"/add"}, method =RequestMethod.POST)
    public JsonResponse getAllUser(
            @RequestParam String input_username,@RequestParam String input_password,
            @RequestParam String input_email,@RequestParam String membership_name,
            @RequestParam String input_full_name,@RequestParam String inputZip,@RequestParam String inputCountry,
            @RequestParam String inputCity,@RequestParam String inputAddress
    ) {
        String address = inputZip+" ,"+inputCity+" "+inputCountry+ " "+inputAddress;
        User user = new User();
        user.setFull_name(input_full_name);
        user.setAddress(address);
        user.setActive(0);
        user.setEmail(input_email);
        user.setMembership(membership_name);
        user.setPassword(input_password);
        user.setUsername(input_username);
        user.setPay_id(payService.generatedPayID(10));
        try {
            userService.saveNormalUser(user);
            mailService.sendmail(user.getFull_name(),user.getPay_id(),user.getEmail());
            return  new JsonResponse(user.getPay_id(),"success");
        }
        catch (Exception e)
        {
            return  new JsonResponse("sikertelen","error");
        }
    }
    @RequestMapping(value = {"/getAll"}, method =RequestMethod.POST)
    public List<User> getAllUser() {
        return userService.findAll();
    }

    @RequestMapping(value = {"/changeStatus"}, method =RequestMethod.POST)
    public JsonResponse getAllUser(@RequestParam String id)
    {
        Integer integer = Integer.valueOf(id);
        User user = userService.findById(integer);
        if(user.getActive()==0)
        {
            user.setActive(1);
        }
        else
        {
            user.setActive(0);
        }
        if(userService.simpleSaveUser(user)){
          return new JsonResponse("Sikeres","success");
        }
        else
        {
          return new JsonResponse("Sikertelen!","error");
        }
    }
    @PostMapping(value = {"/deleteUser/{id}"})
    public JsonResponse deleteUserById(@PathVariable Integer id) {
        User user = userService.findById(id);
        if(userService.deleteUserById(user))
        {
            return new JsonResponse("Sikeres törlés","success");
        }
        else return new JsonResponse("Sikertelen törlés","error");
    }

}
