package tipp_alpha.tipps.controllers.admin_rest;



import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import tipp_alpha.tipps.DTOS.JsonResponse;
import tipp_alpha.tipps.models.Tipp;
import tipp_alpha.tipps.service.TippService;

import java.sql.Timestamp;
import java.util.List;

@RequestMapping( value = "/tipp")
@RestController
public class TippRestController {

    @Autowired
    TippService tippService;

    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(value = {"/add"}, method =RequestMethod.POST)
    public JsonResponse addCategory(
            @RequestParam String input_player1,@RequestParam String input_player2,
            @RequestParam String input_short_description,@RequestParam String input_description,
            @RequestParam String date,@RequestParam String input_hVv,@RequestParam double input_odds
    ) {
        try {
            Tipp tipp = new Tipp();
            tipp.setOdds(input_odds);
            tipp.setHazaVvendeg(input_hVv);
            tipp.setDescription(input_description);
            tipp.setShort_description(input_short_description);
            tipp.setPlayer2(input_player2);
            tipp.setPlayer1(input_player1);
            tipp.setWinOrLose(0);
            tipp.setTipp_generated(Timestamp.valueOf(date));
            tippService.save(tipp);
            return new JsonResponse("","success");
        }
        catch (Exception e)
        {
            return new JsonResponse(e.getMessage(),"error");
        }
    }
    @RequestMapping(value = {"/getAll"}, method =RequestMethod.POST)
    public List<Tipp> getAllTipp() {
        return tippService.findAll();
    }

    @PostMapping(value = "/getClosed")
    public List<Tipp> getClosedTipps()
    {
        return tippService.getClosedTipps();
    }
    @PostMapping(value = "/getOpened")
    public List<Tipp> getOpenedTipps()
    {
        return tippService.getOpenedTipps();
    }
    @PostMapping(value = "/getClosedforAdmin")
    public List<Tipp> getClosedTippsforAdmin()
    {
        return tippService.getClosedTippsforAdmin();
    }
    @PostMapping(value = "/getOpenedforAdmin")
    public List<Tipp> getOpenedTippsforAdmin()
    {
        return tippService.getOpenedTippsforAdmin();
    }
    @PostMapping(value = "/setSuccess/{id}")
    public JsonResponse setSuccessTypeForTipp(@PathVariable Integer id )
    {
        Tipp tipp = tippService.findOneById(id);
        tipp.setWinOrLose(1);
        if(tippService.saveBoolean(tipp)){
            return new JsonResponse("Sikeres módosítás!","success");
        }
        else return new JsonResponse("Sikertelen módosítás","error");
    }
    @PostMapping(value = "/setLose/{id}")
    public JsonResponse setLoseTypeForTipp(@PathVariable Integer id )
    {
        Tipp tipp = tippService.findOneById(id);
        tipp.setWinOrLose(2);
        if(tippService.saveBoolean(tipp)){
            return new JsonResponse("Sikeres módosítás!","success");
        }
        else return new JsonResponse("Sikertelen módosítás","error");
    }
    @PostMapping(value = "/setMoney/{id}")
    public JsonResponse setMoneyTypeForTipp(@PathVariable Integer id )
    {
        Tipp tipp = tippService.findOneById(id);
        tipp.setWinOrLose(3);
        if(tippService.saveBoolean(tipp)){
            return new JsonResponse("Sikeres módosítás!","success");
        }
        else return new JsonResponse("Sikertelen módosítás","error");
    }
    @RequestMapping(value = {"/delete"}, method =RequestMethod.POST)
    public JsonResponse deleteById(@RequestParam Integer id)
    {
        try {
            tippService.deleteById(id);
            return new JsonResponse("Sikeres","success");
        }
        catch (Exception e)
        {
            return new JsonResponse("Sikertelen","error");
        }

    }

}
