package tipp_alpha.tipps.controllers.admin_rest;


import org.joda.time.DateTime;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import tipp_alpha.tipps.DTOS.BlogPostDTO;
import tipp_alpha.tipps.DTOS.JsonResponse;
import tipp_alpha.tipps.models.BlogPost;
import tipp_alpha.tipps.models.Tipp;
import tipp_alpha.tipps.service.BlogService;
import tipp_alpha.tipps.service.TippService;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@RequestMapping( value = "/blog")
@RestController
public class BlogRestController {

    @Autowired
    BlogService blogService;

    @Autowired
    ModelMapper modelMapper;

    @RequestMapping(value = {"/add"}, method =RequestMethod.POST)
    public JsonResponse addCategory(
            @RequestParam String input_name, @RequestParam String input_short_description,
            @RequestParam String input_description
    ) {
        try {
            BlogPost blogPost = new BlogPost();
            blogPost.setPost_description(input_description);
            blogPost.setShort_description(input_short_description);
            blogPost.setPost_title(input_name);
            blogPost.setPost_generated(new Timestamp(System.currentTimeMillis()));
            blogService.save(blogPost);
            return new JsonResponse("","success");
        }
        catch (Exception e)
        {
            return new JsonResponse(e.getMessage(),"error");
        }
    }
    @RequestMapping(value = {"/getAll"}, method =RequestMethod.POST)
    public List<BlogPost> getAllPost() {
        return  blogService.findAll();
    }
    @RequestMapping(value = {"/delete"}, method =RequestMethod.POST)
    public JsonResponse deleteById(@RequestParam Integer id) {

        try{
                blogService.deleteById(id);
                return new JsonResponse("sikeres","success");
        }
        catch (Exception e)
        {
            return  new JsonResponse("sikertelen","error");
        }

    }

}
