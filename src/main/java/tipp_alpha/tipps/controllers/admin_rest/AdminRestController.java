package tipp_alpha.tipps.controllers.admin_rest;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import tipp_alpha.tipps.DTOS.JsonResponse;
import tipp_alpha.tipps.models.Statics;
import tipp_alpha.tipps.models.User;
import tipp_alpha.tipps.service.MailService;
import tipp_alpha.tipps.service.PayService;
import tipp_alpha.tipps.service.StaticService;
import tipp_alpha.tipps.service.UserService;

import javax.validation.Valid;
import java.util.List;

@RequestMapping( value = "/admin-rest")
@RestController
public class AdminRestController {

    @Autowired
    StaticService staticService;

    @RequestMapping(value = {"/static-edit"}, method =RequestMethod.POST)
    public JsonResponse getAllUser(@Valid @ModelAttribute Statics statics)
    {
        if(staticService.save(statics))
        {
           return new JsonResponse("msg","success");
        }
        else return new JsonResponse("msg","error");
    }


}
